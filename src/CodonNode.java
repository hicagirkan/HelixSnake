
public class CodonNode {
	
	String codonName;
	int codonScore;
	CodonNode next;
	
	public CodonNode(String codonName, int codonScore) {
		this.codonName = codonName;
		this.codonScore = codonScore;
	}

	
}
